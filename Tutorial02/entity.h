#pragma once
#include "windows.h"


class entity
{
public:
	entity();

	// get functions for variables
	float getXOffset(void);
	float getYOffset(void);

	// set functions for variables
	void setXOffset(float x);
	void setYOffset(float y);

protected:
	float xOffset;
	float yOffset;

private:

};