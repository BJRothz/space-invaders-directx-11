#pragma once
#include "Invader.h"
#include "windows.h"
#include <iostream>
#include <fstream>

class game
{
public:
	combatEntity player;
	enemy* invaders = new enemy;
	movingEntity* invaderBullet = new movingEntity;
	fstream highscoreFile;

	game();

	~game();

	//get prototypes
	int getNoOfInvaders(void);
	int getScore(void);
	int getHighscore(void);

	//set prototyes
	void setNoOfInvaders(int);
	void setScore(int);
	void setHighscore(int);

	//other functions
	void updateHighscore(void);

protected:
	int noOfInvaders;
	int score;
	int highscore;

};