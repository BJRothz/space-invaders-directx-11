#pragma once
#include "entity.h"
#include "movingEntity.h"
#include "combatEntity.h"

class enemy : public combatEntity
{
public:

	enemy();

	~enemy();

	//get prototypes
	float getFireRate(void);
	bool getAllReloaded(void);

	//set prototypes
	void setFireRate(float x);
	void setAllReloaded(bool cond);

	//functions
	void isOnEdge(void);
	void changeDirection(void);
	void move(void);
	void shoot(void);
	void reload(void);

protected:
	float fireRate;
	bool allReloaded;

};