#include "combatEntity.h"

combatEntity::combatEntity()
{
	fire = 0;
	reloaded = 1;
	alive = 1;
	xOffset = 0.0f;
	yOffset = 0.0f;

	bullet = new movingEntity;

	//bullet->setXOffset(1.5f);
	//bullet->setYOffset(-1.5f);

	speed = 0.0f;
}

combatEntity::~combatEntity()
{
	delete[] bullet;
	bullet = NULL;
}

bool combatEntity::getFire(void)
{
	return fire;
}

bool combatEntity::getReloaded(void)
{
	return reloaded;
}

bool combatEntity::getAlive(void)
{
	return alive;
}

void combatEntity::setFire(bool cond)
{
	fire = cond;
}

void combatEntity::setReloaded(bool cond)
{
	reloaded = cond;
}

void combatEntity::setAlive(bool cond)
{
	alive = cond;
}

void combatEntity::shoot(void)
{
	if (reloaded == 1 && alive == 1)
	{
		bullet->setXOffset(xOffset);
		bullet->setYOffset(yOffset);
		bullet->setSpeed(0.008f);

		reloaded = 0;
	}
}

void combatEntity::reload(void)
{
	bullet->setXOffset(1.0f);
	bullet->setYOffset(-1.0f);
	bullet->setSpeed(0.0f);
	reloaded = 1;
}