#include "Invader.h"

enemy::enemy()
{
	xOffset = 0.0f;
	yOffset = 0.0f;

	direction = 1;
	allReloaded = 1;
}

enemy::~enemy()
{

}

float enemy::getFireRate(void)
{
	return fireRate;
}

bool enemy::getAllReloaded(void)
{
	return allReloaded;
}

void enemy::setFireRate(float x)
{
	fireRate = x;
}

void enemy::setAllReloaded(bool cond)
{
	allReloaded = cond;
}

void enemy::changeDirection(void)
{
	direction = !direction;
	yOffset -= 0.05f;
}

void enemy::move(void)
{
	if (direction == 1)
	{
		speed = 0.001f;
	}
	else if (direction == 0)
	{
		speed = -0.001f;
	}
}

void enemy::shoot(void)
{
	if (reloaded == 1 && allReloaded == 1 && alive == 1)
	{
		bullet->setXOffset(xOffset);
		bullet->setYOffset(yOffset);
		bullet->setSpeed(-0.008f);

		reloaded = 0;
	}
}

void enemy::reload(void)
{
	bullet->setXOffset(1.5f);
	bullet->setYOffset(-1.5f);
	bullet->setSpeed(0.0f);
	reloaded = 1;
}