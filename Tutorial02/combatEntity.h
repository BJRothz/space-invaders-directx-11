#pragma once
#include "movingEntity.h"

class combatEntity : public movingEntity
{
public:
	combatEntity();

	~combatEntity();

	movingEntity* bullet = new movingEntity;

	//get functions for variables
	bool getFire(void);
	bool getReloaded(void);
	bool getAlive(void);

	//set functions for variables
	void setFire(bool cond);
	void setReloaded(bool cond);
	void setAlive(bool cond);

	//other functions
	void shoot(void);
	void reload(void);

protected:
	bool fire;
	bool reloaded;
	bool alive;
};