#pragma once
#include "entity.h"
#include "windows.h"

using namespace std;

class movingEntity : public entity
{
public:

	movingEntity();

	//get function prototypes
	float getSpeed(void);
	bool getDirection(void);

	//set function prototypes
	void setSpeed(float thisSpeed);
	void setDirection(bool cond);

	//functions
	void move(void);

protected:
	float speed;
	bool direction;


};