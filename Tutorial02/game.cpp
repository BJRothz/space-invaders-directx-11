#include "game.h"


game::game()
{
	noOfInvaders = 50;

	invaders = new enemy[50];
	invaderBullet = new movingEntity[50];

	score = 0;
	
	highscoreFile.open("highscore.txt", ios::out);
	highscoreFile >> highscore;
	highscoreFile.close();
}

game::~game()
{
	highscoreFile.open("highscore.txt", ios::in | ios::trunc);
	highscoreFile << highscore;
	highscoreFile.close();

	delete[] invaders;
	invaders = NULL;
}

int game::getNoOfInvaders(void)
{
	return noOfInvaders;
}

int game::getScore(void)
{
	return score;
}

int game::getHighscore(void)
{
	return highscore;
}

void game::setNoOfInvaders(int x)
{
	noOfInvaders = x;
}

void game::setScore(int x)
{
	score = x;
}

void game::setHighscore(int x)
{
	highscore = x;
}

void game::updateHighscore(void)
{
	if (score > highscore)
	{
		highscore = score;
	}
}